# Parte del progetto di Victor

## Lavoro svolto

**Nella parte del progetto**

- Setup dell'ambiente di lavoro
- Tentativo di rendere il suddetto portable
- Creazione dei test di base richiesti
- Creazione di alcuni step riutilizzabili

**Nel progetto in generale**
 
- Miglioramento del sistema di bootstrap del DB
- Abilitazione al raggiungimento da esterno del DB

## Subtree

[Guide](https://gist.github.com/SKempin/b7857a6ff6bddb05717cc17a44091202)
[Aliases](https://docs.gitlab.com/ee/university/training/topics/subtree.html)

