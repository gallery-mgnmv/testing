<?php
require('../templates/header.php');
require('../templates/menu.php');
?>

<?php
if(!isset($user)) {
    die;
}
?>
<div class="container">
    <h1 class="my-4">Update user</h1>
    <form action="index.php?action=edit-user" method="POST">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" value="<?= $user["username"] ?>" required>
        </div>

        <div class="form-check mt-2">
            <label class="form-check-label" for="is_admin">Is Admin</label>
            <input type="checkbox" class="form-check-input" id="is_admin" name="is_admin" <?= $user["is_admin"] ? 'checked' : ''?>>
        </div>

        <input type="hidden" value="<?= $user["id"] ?>" name="id">

        <?php
        if (isset($_SESSION['message'])) {
            ?>
            <div class="text-danger my-auto ms-3"><?= $_SESSION['message']; ?></div>
            <?php
            unset($_SESSION['message']);
        }
        ?>

        <button type="submit" class="btn btn-primary mt-3">Update</button>
    </form>
</div>

<?php
require('../templates/footer.php');
?>
