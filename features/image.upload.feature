Feature: Image upload
  Testing of image upload
  Testing image resize

  @javascript
  Scenario: Upload an image
    Given I am on "https://gallery.zen.pn.it/image/"
    And I am logged in as user "admin" with password "admin" using page "https://gallery.zen.pn.it/login"
    Then I wait 5 seconds