Feature: Home Admin
  Test of the basic functions of the home page



  @javascript
  Scenario: href Checking
    Given I am logged in as user "ad" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    Then I should see a "Gestione immagini" link
    And I should see a "Gestione utenti" link
    And I should see a "ad" button

  @javascript
  Scenario: get admin
    Given I am logged in as user "ad" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    And I follow "Gestione utenti"
    Then I should be on "/index.php?action=list-users"

  @javascript
  Scenario: edit user
    Given I am logged in as user "ad" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    And I follow "Gestione utenti"
    And I press the 2 "Edit" link
    Then I should be on "/index.php?action=edit-user&amp;id=2"


  @javascript
  Scenario: set admin
    Given I am logged in as user "ad" with password "pass" using page "/index.php?action=login"
    And I am on "/index.php?action=edit-user&amp;id=2"
    And I check "is_admin" outside form
    Then I am on "http://localhost:8080/index.php?action=list-users"
    And I log out
    And I am logged in as user "norm" with password "pass" using page "/index.php?action=login"
    And I should see a "Gestione utenti" link


  @javascript
  Scenario: change password
    Given Table "app_user" does not exist
    And I am logged in as user "ad" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    Then I press "navbarDropdown"
    Then I follow "Cambia password"
    And I fill in "newPassword_first" with "pass2"
    And I fill in "newPassword_second" with "pass2"
    And I press "Aggiorna"
    And I log out
    And I am logged in as user "ad" with password "pass2" using page "/index.php?action=login"
    Then I am on site homepage
    And I should see a "Gestione utenti" link