<?php
$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 'homepage';

switch ($action){
    case 'homepage':
        require('../src/controller/homepage.php');
        break;

    case 'login':
        require('../src/controller/Access.php');
        login();
        break;
    case 'login-check':
        require('../src/controller/Access.php');
        loginCheck();
        break;
    case 'logout':
        require('../src/controller/Access.php');
        logout();
        break;

    case 'change-password':
        require('../src/controller/Password.php');
        showForm();
        break;
    case 'set-password':
        require('../src/controller/Password.php');
        setPassword();
        break;

    case 'create-user':
        require('../src/controller/User.php');
        createUser();
        break;
    case 'list-users':
        require('../src/controller/User.php');
        listUsers();
        break;
    case 'edit-user':
        require('../src/controller/User.php');
        updateUser();
        break;
    case 'delete-user':
        require('../src/controller/User.php');
        deleteUser();
        break;
}