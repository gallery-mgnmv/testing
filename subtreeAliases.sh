# Add an alias
# Add
git config alias.sba 'subtree add --prefix referenceSubtree git@gitlab.com:gallery-mgnmv/gallery.git master --squash'
# Pull
git config alias.sbpl 'subtree pull --prefix referenceSubtree git@gitlab.com:gallery-mgnmv/gallery.git master --squash'
# Push
git config alias.sbph 'subtree push --prefix referenceSubtree git@gitlab.com:gallery-mgnmv/gallery.git master'
#
## Adding this subtree adds a st dir with a readme
#git sba
#vi st/README.md
## Edit file
#git status shows differences
#
## Adding, or committing won't change the sub repo at remote
## even if we push
#git add -A
#git commit -m "Adding to subtree readme"
#
## Push to subtree repo
#git sbph
## now we can check our remote sub repo