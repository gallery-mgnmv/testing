Feature: Login
  In order to login
  As a gallery user
  I need to be able to validate the username and password against portal



  @javascript
  Scenario: Login as an existing user and correct password
    Given I am on "/index.php?action=login"
    And Table "app_user" does not exist
    When I fill in "username" with "ad"
    And I fill in "password" with "pass"
    And I press "Accedi"
    Then I should not see "Credenziali non valide."
    And I should be on "/index.php"
    And Table "app_user" has been created


    @javascript
    Scenario: logout
      Given I am on "/index.php?action=login"
      When I fill in "username" with "ad"
      And I fill in "password" with "pass"
      And I press "Accedi"
      Then I should be on "/index.php"
      Then I press "navbarDropdown"
      Then I follow "Esci"
      Then I should be on "/index.php?action=login"
