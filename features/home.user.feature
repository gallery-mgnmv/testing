Feature: Home Admin
  Test of the basic functions of the home page


  @javascript
  Scenario: href checking
    Given I am logged in as user "norm" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    Then I should see a "Gestione immagini" link
    And I should not see a "Gestione utenti" link
    And I should see a "norm" button


  @javascript
  Scenario: change password
    Given Table "app_user" does not exist
    And I am logged in as user "norm" with password "pass" using page "/index.php?action=login"
    And I am on site homepage
    Then I press "navbarDropdown"
    Then I follow "Cambia password"
    And I fill in "newPassword_first" with "pass2"
    And I fill in "newPassword_second" with "pass2"
    And I press "Aggiorna"
    And I log out
    And I am logged in as user "norm" with password "pass2" using page "/index.php?action=login"
    Then I am on site homepage
    And I should see a "Gestione utenti" link