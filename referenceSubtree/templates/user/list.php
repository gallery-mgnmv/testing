<?php
require('../templates/header.php');
require('../templates/menu.php');
?>

<?php
if(!isset($rows)) { // così php storm è in pace e contento :)
    die;
}

$users = $rows->getRows();
$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1;

$len = $rows->recordCount();
$maxPage = (int)($len / 10);
if ($page > $maxPage) {
    $page = $maxPage;
}
if ($page < 1) {
    $page = 1;
}
?>

<div class="container">
    <table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Username</th>
        <th scope="col">Is Admin</th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    <?php
    $i = 0;
    foreach ($users as $user) {
        $i++;
        if ($i <= ($page - 1) * 10) {
            continue;
        }
        if ($i > ($page) * 10) {
            continue;
        }
        ?>
        <tr>
            <th scope="row"><?= $user['id'] ?></th>
            <td><?= $user['username'] ?></td>
            <td><i class="fas <?= $user['is_admin'] ? 'fa-check-circle' : 'fa-times-circle'?>"></i></td>
            <td>
                <a href="index.php?action=edit-user&id=<?php echo $user['id'] ?>" class="btn btn-primary btn-sm">Edit</a>
            </td>

            <td>
                <a href="index.php?action=delete-user&id=<?php echo $user['id'] ?>" class="btn btn-danger btn-sm">Delete</a>
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    </table>

    <div class="row">
        <div class="col">
            <a href="index.php?action=create-user" class="btn btn-success">Add new user</a>
        </div>

        <div class="col">

            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php
                    $start = max($page - 2, 1);
                    for ($i = $start; $i < min($start + 5, $maxPage); $i++) { ?>
                        <li class="page-item <?php echo ($i == $page) ? 'active' : '' ?>"><a class="page-link"
                                                                                             href="?action=list-users&page=<?php echo $i; ?>">
                                <?php echo $i; ?>
                            </a></li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
<?php
require('../templates/footer.php');
?>

