<?php

if (!isset($_SESSION['authorized']) || $_SESSION['authorized']!==true){
    header('Location: index.php?action=login');
    die;
}

if (!$_SESSION['is_admin']) {
    header('Location: index.php');
    die;
}

function createUser() {
    if($_SERVER["REQUEST_METHOD"] === 'GET'){
        require('../templates/user/create.php');
        die;
    }

    global $db;

    $username = $_REQUEST["username"];
    $password = md5($_REQUEST["password"]);
    $is_admin = isset($_REQUEST["is_admin"]);


    if(checkUsernameExists($username)) {
        $_SESSION['message'] = 'username already exists';

        require('../templates/user/create.php');
        die;
    }

    $sql = 'INSERT INTO app_user(username, password, is_admin) VALUES (?, ?, ?)';
    $rows = $db->execute($sql, array($username, $password, $is_admin));

    header('Location: index.php?action=list-users');
}

function listUsers() {
    global $db;

    $sql = 'SELECT * FROM app_user';
    $rows = $db->execute($sql);

    require("../templates/user/list.php");
}

function updateUser() {
    global $db;
    if(!isset($_REQUEST['id'])) {
        header('Location: index.php?action=list-users');
    }

    $id = $_REQUEST['id'];

    if($_SERVER["REQUEST_METHOD"] === 'GET'){
        renderUpdateUserForm($id);
        die;
    }


    $username = $_REQUEST["username"];
    $is_admin = isset($_REQUEST["is_admin"]);

    if(checkUsernameExists($username, $id)) {
        $_SESSION['message'] = 'username already exists';

        renderUpdateUserForm($id);
        die;
    }

    $sql = 'UPDATE app_user SET username = ?, is_admin = ? WHERE id = ?';
    $rows = $db->execute($sql, array($username, $is_admin, $id));

    header('Location: index.php?action=list-users');

}

function deleteUser() {
    global $db;
    if(!isset($_REQUEST['id']) && $_REQUEST['id'] === 1) {
        header('Location: index.php?action=list-users');
    }

    $id = $_REQUEST['id'];

    $sql = 'DELETE FROM app_user WHERE id = ?';
    $rows = $db->execute($sql, array($id));

    header('Location: index.php?action=list-users');
}

function checkUsernameExists($username, $id = null) {
    global $db;

    if (isset($id) ) {
        $sql = 'SELECT username FROM app_user WHERE username = ? AND id != ?';
        $rows = $db->execute($sql, array($username, $id));

    } else {
        $sql = 'SELECT username FROM app_user WHERE username = ?';
        $rows = $db->execute($sql, array($username));
    }


    return $rows->recordCount() > 0;
}

function renderUpdateUserForm($id) {
    global $db;

    $sql = 'SELECT * FROM app_user WHERE id = ?';
    $rows = $db->execute($sql, array($id));

    $user = $rows->getRows()[0];

    require('../templates/user/update.php');
}