<?php
global $dbdtk_dbaccess, $dbdtk_dbhost, $dbdtk_dbuser, $dbdtk_dbpasswd,
       $dbdtk_dbname;

$dbdtk_dbaccess = "mysqli";
$dbdtk_dbhost   = "127.0.0.1";
$dbdtk_dbuser   = "remote";
$dbdtk_dbpasswd = "password";
$dbdtk_dbname   = "gallery";

$ADOConnection = ADONewConnection($dbdtk_dbaccess);
$db = &$ADOConnection;
$db->port = 33066;
$db->debug = true;
$db->Connect($dbdtk_dbhost, $dbdtk_dbuser, $dbdtk_dbpasswd,
    $dbdtk_dbname);
$ADODB_FETCH_MODE = ADODB_FETCH_BOTH;
?>