<?php
require('../templates/header.php');
require('../templates/menu.php');
?>

<div class="container">
    <h1 class="my-4">Create new user</h1>
    <form action="index.php?action=create-user" method="POST">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" class="form-control" id="username" name="username" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <div class="form-check mt-2">
            <label class="form-check-label" for="is_admin">Is Admin</label>
            <input type="checkbox" class="form-check-input" id="is_admin" name="is_admin" value="">
        </div>

        <?php
        if (isset($_SESSION['message'])) {
            ?>
            <div class="text-danger my-auto ms-3"><?= $_SESSION['message']; ?></div>
            <?php
            unset($_SESSION['message']);
        }
        ?>

        <button type="submit" class="btn btn-primary mt-3">Create</button>
    </form>
</div>

<?php
require('../templates/footer.php');
?>
